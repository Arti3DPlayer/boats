django
mysql-python
pillow
south
django-admin-tools
celery 
django-celery
celery-with-redis
sorl-thumbnail
django-password-reset
django-simple-captcha
django-wysiwyg-redactor
whoosh==2.4
django-haystack
django-widget-tweaks

-e git://github.m/krvss/django-social-auth.git#egg=django-social-auth


# скачиваем и разархивируем ( смотрим на https://code.google.com/p/redis/downloads/list последнию версию)
#wget http://redis.googlecode.com/files/redis-2.6.9.tar.gz
#tar -zxvf redis-2.6.9.tar.gz

# устанрвка 
#cd redis-2.6.9
#sudo make
#sudo make test
#sudo make install

# установка сервера
#cd utils
#sudo ./install_server.sh

# проверка роботоспособности
#redis-cli

