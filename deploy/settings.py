# -*- coding: utf-8 -*-
import os
import sys
#import djcelery
#djcelery.setup_loader()

def rel_project(*x):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)

rel_public = lambda *x: rel_project('public', *x)

DEBUG = True
THUMBNAIL_DEBUG = DEBUG
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'proboating_nl',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'proboating_nl',
        'PASSWORD': 'sDg832k4Fh2',
        'HOST': 'localhost',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['http://proboating.ru/',]

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Moscow'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ru'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = '/home/proboating/proboating.ru/public/media/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = '/home/proboating/proboating.ru/public/static/'

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    rel_project('static'),
)
# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'lq8uckdmiby-ocr*b2fp_^^2xt)4a%6y23xcl4(0x)9k7jy57k'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'boats.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'boats.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    rel_project('templates'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth' ,
    'django.contrib.messages.context_processors.messages' ,
    'django.core.context_processors.debug' ,
    'django.core.context_processors.i18n' ,
    'django.core.context_processors.media' ,
    'django.core.context_processors.static' ,
    'django.core.context_processors.request' ,
    'social_auth.context_processors.social_auth_by_name_backends',
)

ADMIN_TOOLS_THEMING_CSS = 'admin_tools/css/theming.css'
ADMIN_TOOLS_INDEX_DASHBOARD = 'boats.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_MENU = 'boats.dashboard.CustomMenu'

BROKER_HOST = "localhost"
BROKER_BACKEND="redis"
REDIS_PORT=6379
REDIS_HOST = "localhost"
BROKER_USER = ""
BROKER_PASSWORD =""
BROKER_VHOST = "0"
REDIS_DB = 0
REDIS_CONNECT_RETRY = True
CELERY_SEND_EVENTS=True
CELERY_RESULT_BACKEND='redis'
CELERY_TASK_RESULT_EXPIRES =  10
CELERYBEAT_SCHEDULER="djcelery.schedulers.DatabaseScheduler"
CELERY_ALWAYS_EAGER=False
CELERY_IMPORTS=('boats.tasks',)

REDACTOR_OPTIONS = {
    'lang': 'ru',
    'focus': 'false',
    'convertDivs': 'false',
    'fileUpload': 'files'
}
REDACTOR_UPLOAD = 'uploads/'

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(os.path.dirname(__file__), 'whoosh_index'),
    },
}


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'haystack',
    'redactor',
    'django.contrib.flatpages',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.admin',
    'south',
    'sorl.thumbnail',
    'social_auth',
    'password_reset',
    'captcha',
    #'djcelery',
    'widget_tweaks',

    'boats.accounts',
    'boats.gallery',
    'boats.articles',
    'boats.catalog',
    'boats.main',
    'boats.threadedcomments',
)

# accounts
AUTH_USER_MODEL = 'accounts.User'
SOCIAL_AUTH_USER_MODEL = 'accounts.User'
ACCOUNTS_MIN_PASSWORD_LENGTH = 4
#LOGIN_REDIRECT_URL = "/home/"
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'proboating.ru@gmail.com'
EMAIL_HOST_PASSWORD = 'newlinepro'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

FACEBOOK_APP_ID = '360469977412811'
FACEBOOK_API_SECRET = '7ea9a43e345407c22f31e404de2e2fcc'

VK_APP_ID = '3734634'
VKONTAKTE_APP_ID = VK_APP_ID
VK_API_SECRET = '3WFLZkFoqwdD6jufIdE9'
VKONTAKTE_APP_SECRET = VK_API_SECRET

TWITTER_CONSUMER_KEY = 'da8hZdv1xAXf2U018BUNYQ'
TWITTER_CONSUMER_SECRET = 'U1ZIxDK80bdGr5SAWeRWYFmYERGdfg7i0kA9rRA8I'
SOCIAL_AUTH_CHANGE_SIGNAL_ONLY = True

import random
# Если имя не удалось получить, то можно его сгенерировать
SOCIAL_AUTH_DEFAULT_USERNAME = lambda: random.choice(['Darth_Vader', 'Obi-Wan_Kenobi', 'R2-D2', 'C-3PO', 'Yoda'])
# Разрешаем создавать пользователей через social_auth
SOCIAL_AUTH_CREATE_USERS = True

# Перечислим pipeline, которые последовательно буду обрабатывать респонс
SOCIAL_AUTH_PIPELINE = (
    # Получает по backend и uid инстансы social_user и user
    'social_auth.backends.pipeline.social.social_auth_user',
    # Получает по user.email инстанс пользователя и заменяет собой тот, который получили выше.
    # Кстати, email выдает только Facebook и GitHub, а Vkontakte и Twitter не выдают
    'social_auth.backends.pipeline.associate.associate_by_email',
    # Пытается собрать правильный username, на основе уже имеющихся данных
    'social_auth.backends.pipeline.user.get_username',
    # Создает нового пользователя, если такого еще нет
    'social_auth.backends.pipeline.user.create_user',
    # Пытается связать аккаунты
    'social_auth.backends.pipeline.social.associate_user',
    # Получает и обновляет social_user.extra_data
    'social_auth.backends.pipeline.social.load_extra_data',
    # Обновляет инстанс user дополнительными данными с бекенда
    'social_auth.backends.pipeline.user.update_user_details'
)

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.twitter.TwitterBackend',
    'social_auth.backends.facebook.FacebookBackend',
    'social_auth.backends.google.GoogleOAuthBackend',
    'social_auth.backends.google.GoogleOAuth2Backend',
    'social_auth.backends.google.GoogleBackend',
    'social_auth.backends.browserid.BrowserIDBackend',
    'social_auth.backends.contrib.vk.VKOAuth2Backend',
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_PROVIDERS = [
    {'id': p[0], 'name': p[1], 'position': {'width': p[2][0], 'height': p[2][1], }}
    for p in (
        ('facebook', u'Login via Facebook', (0, -105)),
        ('vk-oauth', u'Login via VK', (0, 0)),
        ('google', u'Login via Google', (0, -175)),
        ('twitter', u'Login via Twitter', (0, -140)),
    )
]
#end_accounts
CAPTCHA_LENGTH = 5
CAPTCHA_FONT_SIZE = 60
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
