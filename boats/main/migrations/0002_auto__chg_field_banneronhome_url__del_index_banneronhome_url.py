# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'BannerOnHome.url'
        db.alter_column(u'main_banneronhome', 'url', self.gf('django.db.models.fields.URLField')(max_length=200))
        # Removing index on 'BannerOnHome', fields ['url']
        db.delete_index(u'main_banneronhome', ['url'])


    def backwards(self, orm):
        # Adding index on 'BannerOnHome', fields ['url']
        db.create_index(u'main_banneronhome', ['url'])


        # Changing field 'BannerOnHome.url'
        db.alter_column(u'main_banneronhome', 'url', self.gf('django.db.models.fields.SlugField')(max_length=50))

    models = {
        u'main.banneronhome': {
            'Meta': {'object_name': 'BannerOnHome'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'site': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['sites.Site']", 'unique': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['main']