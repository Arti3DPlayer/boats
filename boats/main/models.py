# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.sites.models import Site

class BannerOnHome(models.Model):
    site  = models.OneToOneField(Site, verbose_name=u'Сайт')
    image = models.ImageField(upload_to='banner/', verbose_name=u'Картинка 240х400')
    url   = models.URLField(verbose_name='URL')

    class Meta:
        verbose_name = u'Рекламный баннер'
        verbose_name_plural = u'Рекламный баннер'

    def __unicode__(self):
        return self.image.name