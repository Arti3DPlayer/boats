# -*- coding: utf-8 -*-
from .. decorators import render_to, render_to_json
from django.views.decorators.csrf import csrf_exempt

from django.template import RequestContext
from django.http import Http404,HttpResponse, HttpRequest
from django.shortcuts import render_to_response, HttpResponseRedirect, redirect, get_object_or_404, render
from django.core.urlresolvers import reverse

from django.db.models import Q
from boats.articles.models import Article, Category
from boats.catalog.models import Product, Ad
from boats.catalog.forms import CatalogModelSearchForm, CatalogBoatSearchForm
from boats.gallery.models import Gallery
from django.contrib.contenttypes.models import ContentType
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from models import BannerOnHome


@render_to('base.html')
def home(request):
    articles = Article.objects.order_by('-pub_date')[:5]
    testing = Article.objects.filter(category__id=3).order_by('-pub_date')[:3]
    rate_ads = Ad.objects.filter(active=True).order_by('-views')[:24]
    last_ads = Ad.objects.filter(active=True).order_by('-pub_date')[:6]
    catalog_model_search_form = CatalogModelSearchForm()
    catalog_boat_search_form = CatalogBoatSearchForm()
    rand_gallery = Gallery.objects.all().order_by('?')[0:3]
    banner = BannerOnHome.objects.all()
    if banner:
        banner = banner[0]

    return {
        'articles': articles,
        'testing': testing,
        'rate_ads': rate_ads,
        'last_ads': last_ads,
        'rand_gallery': rand_gallery,
        'banner': banner,
        'CatalogModelSearchForm': catalog_model_search_form,
        'CatalogBoatSearchForm': catalog_boat_search_form,
    }