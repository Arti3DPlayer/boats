# -*- coding: utf-8 -*-
from django.contrib import admin
from models import BannerOnHome
from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.forms import FlatpageForm
from django.contrib.flatpages.admin import FlatPageAdmin
from redactor.widgets import RedactorEditor
from django import forms

class BannerAdmin(admin.ModelAdmin):
    list_display = ('site','image')

class FlatPageAdminForm(FlatpageForm):
    content = forms.CharField(widget=RedactorEditor)
    class Meta:
        model = FlatPage

class MyFlatPageAdmin(FlatPageAdmin):
    form = FlatPageAdminForm

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, MyFlatPageAdmin)
admin.site.register(BannerOnHome, BannerAdmin)