/*gallery to carousel*/
    function galleries(gallery_id) {    
        $(gallery_id).addClass('carousel slide');
        $(gallery_id).attr('data-interval','2500');
        var numOfImg = $(gallery_id+" img").length;
        var switchers = '<ol class="carousel-indicators">';
        for (var i = 0; i < numOfImg; i++) {
            switchers += '<li data-target="'+gallery_id+'" data-slide-to="'+i+'"></li>';
        }
        switchers +='</ol>';        
        $(gallery_id).prepend(switchers);
        $(gallery_id+' li:first').addClass('active');
        $(gallery_id+" img").wrapAll('<div class="carousel-inner"></div>');
        $(gallery_id+" img").wrap('<div class="item"></div>');
        $(gallery_id+" .item:first").addClass('active');
        $(gallery_id).append('<a class="carousel-control left leftKryg" href="'+gallery_id+'" data-slide="prev"><img src="img/kryglaKnopkaLeft.png"></a><a class="carousel-control right rightKryg" style="margin-right: 69px" href="'+gallery_id+'" data-slide="next"><img src="img/kryglaKnopkaRight.png"></a>')
    } 

$(document).ready(function(){
    var numOfGalleriesOnPage = $('.gallery').length;
    if(numOfGalleriesOnPage > 0) {
        $('.gallery').each(function(index){
            $(this).attr('id','myCarousel'+index);
        });
    }

    $(".xrest").mouseover(function(){ //при наведении на крестик
            $(this).addClass('krasnuy'); //делем его красным
    });
	$(".xrest").mouseout(function(){ //при уходе мышки с крестика
            $(this).removeClass('krasnuy'); //делем его НЕкрасным
    });

    $(".pen").mouseover(function(){ //при наведении на крестик
        $(this).addClass('blue'); //делем его красным
    });
    $(".pen").mouseout(function(){ //при уходе мышки с крестика
        $(this).removeClass('blue'); //делем его НЕкрасным
    });
    $('.closes').click(function(){
        $('#dropmenu').fadeOut();
    });
    $('.closes').click(function(){
        $('#dropmenu10').fadeOut();
    });

    $('#smallLogo [rel=tooltip]').tooltip({ placement: 'right' });
    $('[rel=tooltip]').tooltip();
    $('a.register').click(function(){
        $('#dropmenu10').fadeIn();
    });
    $('a.forgotpass').click(function(){
        $('.vupada').slideDown(); 
    });
    
    
    $('#islider').carousel({ 
        interval: 3000,
        pause: "hover"
    });

    //Отключение перехода по якорю #
    $('body').on('click','[href^=#]',function (e) {
        e.preventDefault();
    });

    $('body').on('click','.seruy a',function(){
        $('#dropmenu').fadeIn();
    });

    $('body').on('click','.seruy a',function(){
        $('#dropmenu2').fadeIn();
    });

    $('body').on('mouseover','.uploadFoto .span2',function(){
        $(this).find('.cheku').show();
    });
    $('body').on('mouseout','.uploadFoto .span2',function(){
        $(this).find('.cheku').hide();
    });

    $('body').on('click','.prevy',function(){
        var pic=$(this).attr('data-src');
        $('#sliderId').animate({opacity: 0.1},200).attr('src',pic).animate({opacity: 1},200);
    });
    $('body').on('click','.buttonAvtor',function(){
        $('#dropmenu').fadeIn();
    });
    $('.stranica666 .icon-remove').click(function(){  $('#dropmenu3').fadeOut(); });

});