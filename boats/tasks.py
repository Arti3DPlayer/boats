from celery.task import periodic_task
from datetime import timedelta
from boats.catalog.models import Ad

@periodic_task(run_every = timedelta(hours=24))
def remove_not_active_ads():
    ads = Ad.objects.filter(active=False).delete()