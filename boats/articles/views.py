# -*- coding: utf-8 -*-
from .. decorators import render_to, render_to_json
from django.views.decorators.csrf import csrf_exempt

from django.template import RequestContext
from django.http import Http404,HttpResponse, HttpRequest
from django.shortcuts import render_to_response, HttpResponseRedirect, redirect, get_object_or_404, render
from django.core.urlresolvers import reverse

from django.db.models import Q
from boats.articles.models import Article, Category
from boats.catalog.models import Product, Ad
from django.contrib.contenttypes.models import ContentType
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
import re


def articles_show(request,slug):
    if slug=='all':
        articles = Article.objects.order_by('-pub_date')
    else:
        articles = Article.objects.filter(category__slug=slug).order_by('-pub_date')

    categories = Category.objects.order_by('-priority')
    paginator = Paginator(articles, 12)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page(1)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    context = {
        'articles': articles,
        'categories': categories,
    }
    
    if request.session.get('articles_display'):
        s = request.session['articles_display']
        if s == 'list':
            return render(request, 'articles/articles_list.html', context)
        elif s == 'tiles':
            return render(request, 'articles/articles_tiles.html', context)
            
    return render(request, 'articles/articles_tiles.html', context)

@render_to('articles/article_details.html')
def article_details(request,slug):
    article = get_object_or_404(Article, slug=slug)

    #находит похожие статьи по тегам разделённым запятой
    for tag in article.tags.replace(' ', '').split(','):
        try:
            query = query | Q(tags__icontains = tag)
        except NameError:
            query = Q(tags__icontains = tag)
    sub_articles = Article.objects.exclude(id = article.id).filter(query)[:4]
    #находит похожие статьи по категориям, если найденных по тегам меньше 4
    if sub_articles.count() < 4:
        sub_articles = Article.objects.exclude(id= article.id).filter(query | Q(category=article.category))[:4]

    categories = Category.objects.all()
    return {
        'categories': categories,
        'article': article,
        'sub_articles': sub_articles,
    }


def articles_display(request,slug):
    if request.session.get('articles_display'):
        s = request.session['articles_display']
        if slug=="list":
            s = "list"
        else:
            s = "tiles"
    else:
        s = "list"

    request.session['articles_display'] = s
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@render_to('search.html')
@csrf_exempt
def search(request):
    er = ''
    results = {}
    count = 0
    search_words = ""
    types = ContentType.objects.get_for_models(Article, Ad, Product,)
    if 'search_words' in request.POST and len(request.POST.get('search_words')):
        search_words = re.sub(" +"," ",request.POST['search_words']).strip(" ")
        for tag in search_words.split(" "):
            try:
                query = query | Q(title__icontains = tag) | Q(text__icontains=search_words)
            except NameError:
                query = Q(tags__icontains = tag) | Q(title__icontains = tag) | Q(text__icontains=search_words)
        results = Article.objects.filter(query)
        count = results.count()
        if not len(results):
            er = u'По вашему запросу не найдено ни одной записи.'
        results = Paginator(results, 20)
        page = request.GET.get('page')
        try:
            results = results.page(page)
        except PageNotAnInteger:
            results = results.page(1)
        except EmptyPage:
            results = results.page(paginator.num_pages)
    return {
        'search_words': search_words,
        'count': count,
        'er': er,
        'results': results,
    }
