# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings


urlpatterns = patterns('boats.articles.views',
    url(r'category/(?P<slug>\w+)/$', 'articles_show', name='articles_show'),
    url(r'display/(?P<slug>\w+)/$', 'articles_display', name='articles_display'),
    url(r'(?P<slug>\w+)/$', 'article_details', name='article_details'),
)