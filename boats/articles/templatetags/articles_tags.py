# -*- coding: utf-8 -*-
from django.conf import settings
from django import template
from django.utils.translation import ugettext_lazy as _
from django.contrib.flatpages.models import FlatPage
from django.db.models import Q, F, Avg, Count

from boats.articles import models

register = template.Library()

@register.inclusion_tag('articles/inclusion/footer_articles_category_list.html')
def articles_category_footer_list_tag():
    c = models.Category.objects.order_by('-priority')
    return {
        'article_categories': c
        }

@register.inclusion_tag('articles/inclusion/top_menu_articles_category_list.html', takes_context=True)
def top_menu_articles_category_list_tag(context):
    c = models.Category.objects.filter(show_on_top=True).order_by('-priority')
    return {
        'top_article_categories': c,
        'is_authenticated': context['request'].user.is_authenticated()
        }