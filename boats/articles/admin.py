from django.contrib import admin
from boats.articles.models import Article, Category

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title',)

class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title','pub_date','category', 'description', 'author')
    search_fields = ('title', 'text', 'author')
    list_filter = ('pub_date',)

admin.site.register(Category,CategoryAdmin)
admin.site.register(Article,ArticleAdmin)