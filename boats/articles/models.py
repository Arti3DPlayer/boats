# -*- coding: utf-8 -*-
from django.db import models
from redactor.fields import RedactorField

class Category(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Название категории', help_text=u'Например: Путешествия')
    slug  = models.CharField(max_length=200, unique=True, verbose_name=u'Название категории [транслит]', help_text=u'Будет использоваться в url. Например: travel')
    show_on_top = models.BooleanField(default=False, verbose_name=u'Показывать в основном меню')
    priority = models.IntegerField(default=1,blank = True, null = True, verbose_name=u'Приоритет')

    class Meta:
        verbose_name = u'Категории'
        verbose_name_plural = u'Категории'

    def __unicode__(self):
        return self.title

class Article(models.Model):
    title       = models.CharField(max_length=30, verbose_name=u'Заголовок',  help_text=u'Например: Спортивная яхта')
    slug        = models.CharField(max_length=30, unique=True, verbose_name=u'Заголовок [транслит]', help_text=u'Будет использоваться в url. Например: sportivna_yahta')
    category    = models.ForeignKey(Category, verbose_name=u'Категория')
    description = models.TextField(verbose_name=u'Краткое описание', help_text=u'Рекомендуется не более трех-четырех строк.')
    text        = RedactorField(verbose_name=u'Текст новости')
    author      = models.CharField(max_length=30, verbose_name=u'Автор статьи')
    pub_date    = models.DateTimeField(auto_now=True, auto_now_add=True, verbose_name=u'Дата публикации')
    preview     = models.ImageField(verbose_name=u'Логотип', upload_to='articles/', null = True,)
    source      = models.URLField(max_length=200, blank=True, verbose_name=u'Источник статьи(ссылка)', help_text=u'Не обязательно')
    tags        = models.CharField(max_length=300, verbose_name=u'Теги',  help_text=u'Теги вводяться через запятую. Например: спортивный, мощный, катер')

    class Meta:
        verbose_name = u'Новость/Статья'
        verbose_name_plural = u'Новости и статьи'

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/articles/%s/%s/"%(self.category.slug, self.slug)

    
