# -*- coding: utf-8 -*-
"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'boats.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'boats.dashboard.CustomAppIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name
from django.core.urlresolvers import reverse

from admin_tools.menu import items, Menu

class CustomMenu(Menu):
    def __init__(self, **kwargs):
        super(CustomMenu, self).__init__(**kwargs)
        self.children += [
            items.MenuItem('Обзор', reverse('admin:index')),
            items.MenuItem('Новости и статьи', '/admin/articles/article/'),
            items.MenuItem('Каталог', '/admin/catalog/product/'),
            items.MenuItem('Обьявления', '/admin/catalog/ad/'),
            items.AppList('Остальные приложения'),
        ]


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for boats.
    """
    title = u'Обзор'
    columns = 3

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        self.children.append(
            modules.ModelList(
                title = u'Админстратор',
                models=(
                    'boats.accounts.models.User',
                    'django.contrib.auth.*',
                    'django.contrib.sites.*',
                    'boats.threadedcomments.models.ThreadedComment',
                    'boats.main.models.BannerOnHome',
                    'django.contrib.flatpages.*',
                ),
            )
        )

        self.children.append(modules.Group(
            title=u"Информация",
            display="tabs",
            children=[
                modules.AppList(
                    title=u'Новости и статьи',
                    models=('boats.articles.models.*','boats.gallery.models.*',)
                ),
                modules.AppList(
                    title=u'Каталог и Обьявления',
                    models=('boats.catalog.models.*',)
                )
            ]
        ))

        self.children.append(modules.RecentActions(_('Recent Actions'), 5))
