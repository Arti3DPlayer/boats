��          �      |      �  f   �     X  C   ^  ^   �               %     6     H     h          �  k   �          0     P     e     n  5   �  Q   �  V     �  _  �   
     �  z   �  �   R     		  4   !	  ,   V	  )   �	  9   �	  R   �	  ,   :
  ,   g
  �   �
  R   o  +   �  7   �  
   &     1  h   I  r   �  �   %                  
                                                       	                        An email was sent to <strong>%(email)s</strong> %(ago)s ago. Use the link in it to set a new password. Email Hi, <strong>%(username)s</strong>. Please choose your new password. If you don't want to reset your password, simply ignore this email and it will stay unchanged. New password New password (confirm) New password set Password recovery Password recovery on %(domain)s Password recovery sent Recover my password Set new password Sorry, this password reset link is invalid. You can still <a href="%(recovery_url)s">request a new one</a>. Sorry, this user doesn't exist. The two passwords didn't match. Unable to find user. Username Username or Email You can set your new password by following this link: You or someone pretending to be you has requested a password reset on %(domain)s. Your password has successfully been reset. You can use it right now on the login page. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-08-01 15:34+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Письмо было отправлено в <strong>%(email)s</strong> %(ago)s назад. Используйте ссылку вв нем, чтобы установить новый пароль. Email Здраствуйте, <strong>%(username)s</strong>. Пожалуйста введите ваш новый пароль. Если вы не хотите изменить свой пароль, просто игнорируйте это письмо, и он останется без изменений. Новый пароль Новый пароль (подтверждение) Установить новый пароль Восстановление пароля Восстановление пароля на %(domain)s Письмо для восстановления пароля отправлено Восстановить мой пароль Установить новый пароль К сожалению, эта ссылка сброса пароля является недействительной. Вы все еще можете <a href="%(recovery_url)s">запросить еще одну</a>. Извините, такого пользователя не существует. Два пароля не совпадают Не удалось найти пользователя Логин Логин или Email Вы можете установить новый пароль, пройдя по этой ссылке: Вы или кто-то, притворяясь вами запросил сброс пароля на %(domain)s. Ваш пароль был успешно изменен. Вы можете использовать его прямо сейчас на странице авторизации. 