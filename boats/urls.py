# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from boats.feeds import LatestArticles
from django.views.generic.base import RedirectView
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url='/home/')),
    url(r'^home/$', 'boats.main.views.home'),
    url(r'^gallery/', include('boats.gallery.urls')),
    url(r'^articles/', include('boats.articles.urls')),
    url(r'^catalog/', include('boats.catalog.urls')),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^search/', include('haystack.urls')),
    url(r'^comments/', include('boats.threadedcomments.urls')),
    url(r'^accounts/', include('boats.accounts.urls')),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^redactor/', include('redactor.urls')),
    url(r'', include('social_auth.urls')),
    url(r'^feeds/articles/$',LatestArticles()),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT,}),
   )