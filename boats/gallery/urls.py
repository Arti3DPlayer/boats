# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url, include

urlpatterns = patterns('boats.gallery.views',
    url(r'^all/$', 'gallery_list', name = "gallery_all"),
    url(r'^get/(?P<slug>\w+)/$', 'one_gallery', name = "one_gallery"),
    url(r'^load_more/$', 'load_more', name = "load_more_galeries"),
)
