# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Gallery.author'
        db.delete_column(u'gallery_gallery', 'author_id')


    def backwards(self, orm):
        # Adding field 'Gallery.author'
        db.add_column(u'gallery_gallery', 'author',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['accounts.User']),
                      keep_default=False)


    models = {
        u'gallery.gallery': {
            'Meta': {'object_name': 'Gallery'},
            'description': ('redactor.fields.RedactorField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'gallery.item': {
            'Meta': {'object_name': 'Item'},
            'gallery': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gallery.Gallery']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['gallery']