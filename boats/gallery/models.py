# -*- coding: utf-8 -*-

from django.db import models
from redactor.fields import RedactorField
from boats.accounts.models import User
from random import choice

class Gallery(models.Model):
    title           = models.CharField(max_length=200, verbose_name=u'Название галереи')
    slug            = models.CharField(max_length=30, unique=True, verbose_name=u'Название галереи [транслит]', help_text=u'Будет использоваться в url. Например: travel')
    description     = RedactorField(verbose_name=u'Описание')
    pub_date        = models.DateTimeField(auto_now=True, auto_now_add=True)
    author          = models.ForeignKey(User, verbose_name=u'Автор')

    class Meta:
        verbose_name = u'Галерея'
        verbose_name_plural = u'Галереи'

    def __unicode__(self):
        return self.title

    @property
    def get_images(self):
        return Item.objects.filter(gallery = self)

    @property
    def get_random_image(self):
        items = Item.objects.filter(gallery = self)
        if items:
            return choice(items)
        else:
            return None

class Item(models.Model):
    img = models.ImageField(verbose_name=u'Фото', upload_to='gallery/')
    gallery = models.ForeignKey(Gallery)

    class Meta:
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'

    def __unicode__(self):
        return self.img.name