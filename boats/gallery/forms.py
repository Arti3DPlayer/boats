# -*- coding: utf-8 -*-
from django.utils.safestring import mark_safe
from django import forms
from models import Item

class AdminImageWidget(forms.FileInput):
    """
    A ImageField Widget for admin that shows a thumbnail.
    """
    def __init__(self, attrs={}):
        super(AdminImageWidget, self).__init__(attrs)
    def render(self, name, value, attrs=None):
        output = []
        if value and hasattr(value, "url"):
            output.append((u'<a target="_blank" href="%s"><img src="%s" style="height: 100px;" /></a>' % (value.url, value.url)))
        output.append(super(AdminImageWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))

class ImagePreviewForm(forms.ModelForm):
    #image = forms.FileField(label=u"Photo",widget=AdminImageWidget)
    class Meta:
        model = Item
        widgets = {'img':AdminImageWidget}