# -*- coding: utf-8 -*-
from django.contrib import admin
from models import Gallery, Item
from forms import ImagePreviewForm
from boats.accounts.models import User

class ItemInline(admin.StackedInline):
    model = Item
    form = ImagePreviewForm
    extra = 0

class GallerytAdmin(admin.ModelAdmin):
    list_display = ('title','author', 'pub_date')
    list_filter = ('title', 'author')
    ordering = ('-pub_date',)
    inlines = [ItemInline,]
    exclude = ('author',)

    def save_model(self, request, obj, form, change):
        try:
            getattr(obj, 'author', 0)
        except User.DoesNotExist:
            obj.author = request.user
        obj.save()

admin.site.register(Gallery, GallerytAdmin)