# -*- coding: utf-8 -*-
from .. decorators import render_to, render_to_json
from models import Gallery, Item
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers
from django.shortcuts import render_to_response, get_object_or_404

@render_to("gallery/gallery_list.html")
def gallery_list(request):
    galleries = Gallery.objects.all().order_by('-pub_date')
    galleries = Paginator(galleries, 6)
    page = request.GET.get('page', 1)
    try:
        cur_list = galleries.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        cur_list = galleries.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        cur_list = galleries.page(paginator.num_pages)
    return {
        'galleries': cur_list
    }

@render_to("gallery/one_gallery.html")
def one_gallery(request, slug):
    gallery = get_object_or_404(Gallery, slug=slug)
    return {
        'gallery': gallery
    }

def load_more(request):
    try:
        page = int(request.GET['page'])
    except :
        raise Http404
    galleries = Gallery.objects.all().order_by('-pub_date')
    galleries = Paginator(galleries, 6)
    try:
        cur_list = galleries.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        cur_list = galleries.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        cur_list = galleries.page(paginator.num_pages)
    return render_to_response("gallery/includes/more.html",{
        'galleries':cur_list,
         }
    )