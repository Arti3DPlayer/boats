# -*- coding: utf-8 -*-
from django.contrib import auth
from django.contrib.auth import login
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm
from django.http.response import HttpResponseRedirect
from django.shortcuts import resolve_url, render_to_response
from django.utils.http import is_safe_url
from forms import NewUserForm, PasswordChangeForm, LoginForm, UserChangeForm, DivErrorList, SendMailForm
from .. decorators import render_to, render_to_json
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from models import User, Bookmark, BOOKMARK_APPS
from hashlib import sha1
from django.core.mail import send_mail
from datetime import date
from django.http import Http404
from django.contrib.messages import info
from django.contrib.flatpages.models import FlatPage
from django.contrib.auth import authenticate, login
from django.contrib.auth.backends import ModelBackend
from django.contrib.contenttypes.models import ContentType
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from boats.articles.models import Article
from boats.catalog.models import Product, Ad
from boats.gallery.models import Gallery

@csrf_exempt
def authorize(request):
    if request.method=='POST' :
        if 'login-form' in request.POST:
            form_login = AuthenticationForm(data=request.POST or None, error_class = DivErrorList)
            if form_login.errors:
                request.session['form_login_errors'] = u"Пожалуйста, введите корректные имя пользователя и пароль. Оба поля могут быть чувствительны к регистру."
            if form_login.is_valid():
                username = request.POST['username']
                password = request.POST['password']
                user = authenticate(username=username, password=password)
                if user is not None and user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(request.META['HTTP_REFERER'])
                else:
                    request.session['form_login_errors'] = u"Пожалуйста, введите корректные имя пользователя и пароль. Оба поля могут быть чувствительны к регистру."
            return HttpResponseRedirect(request.META['HTTP_REFERER'])
        else:
            raise Http404
    else:
        raise Http404

@login_required(login_url = "/accounts/about/")
@render_to("accounts/profile_change.html")
@csrf_exempt
def get_form_change(request):
    form_change = UserChangeForm(request.POST or None, request.FILES or None,
                        instance=request.user, error_class = DivErrorList)
    if request.method == "POST" and form_change.is_valid():
        form_change.save()
    return {
        'form_change': form_change,
    }

@login_required(login_url = "/accounts/about/")
@render_to("accounts/profile_change_password.html")
@csrf_exempt
def get_form_change_password(request):
    form_change_password = PasswordChangeForm(request.POST or None, request.FILES or None,
                        instance=request.user, error_class = DivErrorList)
    if request.method == "POST" and form_change_password.is_valid():
        form_change_password.save()
    return {
        'form_change_password': form_change_password,
    }

@render_to("accounts/account_confirm.html")
def confirm_registration(request, link):

    try:
        id = int(link[0:link.find("L")])
    except:
        return {
            'msg': u'Неправильная ссылка',
        }
    try:
        user = User.objects.get(id=id)
    except User.DoesNotExist:
        raise Http404

    if user.is_active:
        raise Http404
    else:
        hash_key = sha1()
        hash_key.update("%d:%s:%s" % (user.id, user.username, date.today()))
        if link[link.find("L")+1:]==hash_key.hexdigest():
            user.is_active=True
            user.save()
            user.backend = "%s.%s" % (ModelBackend.__module__, ModelBackend.__name__)
            login(request, user)
        else:
            raise Http404
    return HttpResponseRedirect('/accounts/change/')

@login_required(login_url = "/accounts/about/")
@render_to_json
def add_bookmark(request, content_type, object_id):
    try:
        path = request.GET['path']
    except :
        raise Http404
    try:
        type = ContentType.objects.get(app_label=BOOKMARK_APPS[content_type], model=content_type)
        bookmark = Bookmark.objects.get(content_type = type, url = path, user = request.user)
        bookmark.delete()
        return u"Добавить в избранное"
    except Bookmark.DoesNotExist:
        type = ContentType.objects.get(app_label=BOOKMARK_APPS[content_type], model=content_type)
        Bookmark.objects.create(url = path, user = request.user, content_type=type, object_id=object_id)
        return u"Убрать из избранного"
    return "WTF?! It's a bad!!!"

@render_to("base.html")
def view_home(request):
    return {
        'msg': "Все ОК",
    }

@login_required(login_url = "/accounts/about/")
@render_to("accounts/bookmarks.html")
def show_bookmarks(request):
    
    ad_type = ContentType.objects.get_for_model(Ad)
    product_type = ContentType.objects.get_for_model(Product)
    article_type = ContentType.objects.get_for_model(Article)
    gallery_type = ContentType.objects.get_for_model(Gallery)
    bookmarks_in_ad = Bookmark.objects.filter(content_type=ad_type, user=request.user).prefetch_related().order_by('-object_id')
    bookmarks_in_product = Bookmark.objects.filter(content_type=product_type, user=request.user).prefetch_related().order_by('-object_id')
    bookmarks_in_articles = Bookmark.objects.filter(content_type=article_type, user=request.user).prefetch_related().order_by('-object_id')
    bookmarks_in_gallery = Bookmark.objects.filter(content_type=gallery_type, user=request.user).prefetch_related().order_by('-object_id')
    #############
    try:
        num_ads = int(request.session['ads_on_page'])
    except:
        num_ads = 10
    try:
        num_products = int(request.session['products_on_page'])
    except:
        num_products = 10
    #############
    ads_on_page = request.GET.get('ads_on_page', num_ads)
    request.session['ads_on_page'] = ads_on_page
    products_on_page = request.GET.get('products_on_page', num_products)
    request.session['products_on_page'] = products_on_page
    #############
    bookmarks_in_ad = Paginator(bookmarks_in_ad, ads_on_page)
    bookmarks_in_product = Paginator(bookmarks_in_product, products_on_page)
    ad_page = request.GET.get('ad_page',1)
    product_page = request.GET.get('product_page',1)
    #############
    try:
        bookmarks_in_ad = bookmarks_in_ad.page(ad_page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        bookmarks_in_ad = bookmarks_in_ad.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        bookmarks_in_ad = bookmarks_in_ad.page(paginator.num_pages)
    #############
    try:
        bookmarks_in_product = bookmarks_in_product.page(product_page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        bookmarks_in_product = bookmarks_in_product.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        bookmarks_in_product = bookmarks_in_product.page(paginator.num_pages)

    return {
        'bookmarks_in_ad': bookmarks_in_ad,
        'bookmarks_in_product': bookmarks_in_product,
        'bookmarks_in_articles': bookmarks_in_articles,
        'bookmarks_in_gallery': bookmarks_in_gallery,
    }

@login_required(login_url = "/accounts/about/")
@render_to("accounts/my_ads.html")
def show_my_ads(request):
    ads = Ad.objects.filter(author = request.user, active = True).order_by('-pub_date')
    try:
        num_ads = int(request.session['my_ads_on_page'])
    except:
        num_ads = 10
    ads_on_page = request.GET.get('my_ads_on_page', num_ads)
    request.session['my_ads_on_page'] = ads_on_page
    page = request.GET.get('page',1)
    ads = Paginator(ads, ads_on_page)
    try:
        ads = ads.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        ads = ads.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        ads = ads.page(ads.num_pages)
    return {
        'ads':ads,
    }

@login_required(login_url = "/accounts/about/")
@render_to_json
def del_my_ad(request, object_id):
    try:
        ad = Ad.objects.get(id = object_id)
    except Ad.DoesNotExist:
        raise Http404
    if ad.author != request.user:
        raise Http404
    ad.delete()
    return "Ok"

@render_to("base.html")
def view_home(request):
    return {
        'msg': "Все ОК",
    }

@render_to("accounts/about_signup.html")
def about_signup(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    form_login = AuthenticationForm(data=request.POST or None, error_class = DivErrorList)
    if request.method=='POST' :
        form_signup = NewUserForm(request.POST or None, error_class = DivErrorList)
        if 'password' in request.POST:
            if form_login.errors:
                form_login = LoginForm(data=request.POST, error_class = DivErrorList)
            if form_login.is_valid():
                username = request.POST['username']
                password = request.POST['password']
                user = authenticate(username=username, password=password)
                if user is not None and user.is_active:
                    login(request, user)
                    next_page = request.GET.get('next', "/home/")
                    return HttpResponseRedirect(next_page)
                else:
                    form_login.errors.__dict__['msg']=u"Пожалуйста, введите корректные имя пользователя и пароль. Оба поля могут быть чувствительны к регистру."
        else:
            if 'password2' in request.POST and 'confirm' in request.POST:
                if form_signup.is_valid():
                    form_signup = form_signup.cleaned_data
                    user = User.objects.create_user(username=form_signup['username'], email=form_signup['email'], phone=form_signup['phone'], password=form_signup['password1'])
                    user.is_active = False
                    user.save()
                    hash_key = sha1()
                    hash_key.update("%d:%s:%s" % (user.id, user.username, date.today()))
                    link = "http://localhost:8000/accounts/confirm_registration/%dL%s" % (user.id, hash_key.hexdigest())
                    msg = "Ссылка для подтверждения регистрации - %s" %(link)
                    send_mail("Proboating.ru", msg, "proboating.ru@gmail.com", [user.email])
                    return {'message_after_signup': u'На вашу электронную почту отправлено письмо с ссылкой для \
                             подтверждения регистрации. Ссылка действительна до 24:00',}
    else:
        form_signup = NewUserForm()
    flatpage = FlatPage.objects.get(url="/about/signup/")
    return {
        'path': request.path,
        'flatpage': flatpage,
        'form_signup': form_signup,
        'form_login': form_login,
    }

@csrf_exempt
def send_mail_to_author(request, id):
    if request.method=='POST':
        try:
            test = request.POST['test']
            return render_to_response("accounts/mail/sendmail_form.html",{
                    'send_mail_form': SendMailForm(),
                    }
                )
        except KeyError:
            pass
        send_mail_form = SendMailForm({
            'name': request.POST.get('name'),
            'email': request.POST.get('email'),
            'text': request.POST.get('text'),
            'captcha_0': request.POST.get('captcha_0'),
            'captcha_1': request.POST.get('captcha_1'),
            'copy_to_me': request.POST.get('copy_to_me'),
        }, error_class = DivErrorList)
        if send_mail_form.is_valid():
            try:
                ad = Ad.objects.get(id = id)
            except Ad.DoesNotExist:
                return render_to_response("accounts/mail/sendmail_form.html",{
                    'error': "Не удалось отправить письмо",
                    'send_mail_form': SendMailForm(),
                    }
                )
            title = u"Proboating.ru. Пользователь %s прислал вам сообщение."% (send_mail_form['name'].value())
            msg = send_mail_form['text'].value()
            destination = [ad.author.email,]
            if send_mail_form['copy_to_me'].value():
                destination.append(send_mail_form['email'].value())
            send_mail(title, msg, "proboating.ru@gmail.com", destination)
            return render_to_response("accounts/mail/success.html",{
                'send_mail_form': send_mail_form,
                }
            )
        else:
            return render_to_response("accounts/mail/sendmail_form.html",{
                'send_mail_form': send_mail_form,
                }
            )
    else:
        raise Http404