# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url, include
from django.contrib.auth.views import login, logout
from django.contrib.auth.views import password_reset
from django.views.generic import TemplateView
from forms import LoginForm

urlpatterns = patterns('',
    url(r'^success/$', TemplateView.as_view(template_name='accounts/success.html')),
    url(r'^confirm_registration/(?P<link>.+)/$', 'boats.accounts.views.confirm_registration'),
    url(r'^about/', 'boats.accounts.views.about_signup'),
    url(r'^change/$', 'boats.accounts.views.get_form_change'),
    url(r'^change/password/$', 'boats.accounts.views.get_form_change_password'),
    url(r'^login/$', 'boats.accounts.views.authorize'),
    url(r'^logout/$', logout, {'template_name': 'accounts/logged_out.html'}),
    url(r'^password_reset/', include('password_reset.urls')),
    url(r'^bookmarks/$', 'boats.accounts.views.show_bookmarks'),
    url(r'^my_ads/$', 'boats.accounts.views.show_my_ads'),
    url(r'^del_my_ad/(?P<object_id>\d+)/$', 'boats.accounts.views.del_my_ad'),
    url(r'^send_mail_to_author/(?P<id>\d+)/$', 'boats.accounts.views.send_mail_to_author', name="send_mail_to_author"),
    url(r'^add_bookmark/(?P<content_type>\w+)/(?P<object_id>\d+)/$', 'boats.accounts.views.add_bookmark', name="add_bookmark"),
)
