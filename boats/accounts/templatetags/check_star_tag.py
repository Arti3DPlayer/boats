# -*- coding: utf-8 -*-
from django import template
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import login_required
from boats.accounts.models import Bookmark, BOOKMARK_APPS

register = template.Library()

@login_required(login_url = "/accounts/about/")
@register.inclusion_tag('accounts/star&link.html', takes_context=True)
def check_star(context, content_type, object_id):
    div_class = ""
    url = "/accounts/add_bookmark/%s/%d/" %(content_type, object_id)
    text = ""
    try:
        type = ContentType.objects.get(app_label=BOOKMARK_APPS[content_type], model=content_type)
        Bookmark.objects.get(content_type = type, url=context['request'].path ,user = context['request'].user)
        text = u"Убрать из избранного"
    except Bookmark.DoesNotExist:
        div_class = "polnaStars"
        text = u"Добавить в избранное"

    return {
        'div_class': div_class,
        'url': url,
        'text': text,
        'path': context['request'].path,
    }