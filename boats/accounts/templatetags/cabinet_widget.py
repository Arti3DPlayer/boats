# -*- coding: utf-8 -*-
from django import template
from django.views.decorators.csrf import csrf_exempt
from boats.accounts.forms import NewUserForm, LoginForm, DivErrorList
from django.contrib.auth.forms import AuthenticationForm
from boats.accounts.models import User
from hashlib import sha1
from django.core.mail import send_mail
from datetime import date
from django.contrib.auth import authenticate, login
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect

register = template.Library()
#errors = request.session.pop('boat_form_errors')
@csrf_exempt
@register.inclusion_tag('accounts/cabinet_widget.html', takes_context=True)
def cabinet_widget(context):
    request = context['request']
    if request.user.is_authenticated():
        return {}
    form_signup = NewUserForm()
    form_login = AuthenticationForm()
    if request.session.has_key('form_login_errors'):
        form_login = LoginForm()
        del request.session['form_login_errors']
    if request.method=='POST' :
        if 'reg-form' in request.POST:
            form_signup = NewUserForm(request.POST or None, error_class = DivErrorList)
            if form_signup.is_valid():
                form_signup = form_signup.cleaned_data
                user = User.objects.create_user(username=form_signup['username'], email=form_signup['email'], phone=form_signup['phone'], password=form_signup['password1'])
                user.is_active = False
                user.save()
                hash_key = sha1()
                hash_key.update("%d:%s:%s" % (user.id, user.username, date.today()))
                link = "http://localhost:8000/accounts/confirm_registration/%dL%s" % (user.id, hash_key.hexdigest())
                msg = "Ссылка для подтверждения регистрации - %s" %(link)
                send_mail("Proboating.ru", msg, "proboating.ru@gmail.com", [user.email])
                return {'message_after_signup': u'На вашу электронную почту отправлено письмо с ссылкой для \
                         подтверждения регистрации. Ссылка действительна до 24:00',}
    return {
        'form_signup': form_signup,
        'form_login': form_login,
        'path': request.path,
    }