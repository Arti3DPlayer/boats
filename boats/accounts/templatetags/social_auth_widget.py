from django import template
from django.conf import settings


def social_auth_widget(context):
    try:
        return {
            'path': context['path'],
            'providers': settings.SOCIAL_AUTH_PROVIDERS,
        }
    except KeyError:
        return {
            'path': context['request'].path,
            'providers': settings.SOCIAL_AUTH_PROVIDERS,
        }

register = template.Library()
register.inclusion_tag('social_auth/social_auth_widget.html', takes_context=True)(social_auth_widget)