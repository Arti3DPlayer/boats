# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.dispatch import receiver
from social_auth.signals import socialauth_registered

from django.db.models.signals import pre_save
import random

BOOKMARK_APPS = {
    "article": "articles",
    "ad"     : "catalog",
    "product": "catalog",
    "gallery": "gallery",
}



AbstractUser._meta.get_field('email')._unique = True
AbstractUser._meta.get_field('email').blank = False

class User(AbstractUser):
    phone = models.CharField(_(u'phone'), max_length=15, blank=True)

@receiver(pre_save, sender = User, dispatch_uid="1234")
def new_users_handler(sender, **kwargs):
    if not kwargs['instance'].email:
        first = 'default%s@default.com' % format(random.randint(0,1000000000), '06x')
        try:
            u = User.objects.get(email = first)
            while u:
                first = 'default%s@default.com' % format(random.randint(0,1000000000), '06x')
                u = User.objects.get(email = first)
        except (User.DoesNotExist):
            kwargs['instance'].email = first


class Bookmark(models.Model):
    url = models.URLField(verbose_name="Закладки")
    user = models.ForeignKey(User, related_name = "bookmarks")
    content_type = models.ForeignKey(ContentType, related_name = "rel_item")
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return self.url