# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField, AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from captcha.fields import CaptchaField
from django.forms.util import ErrorList
from django.utils.safestring import mark_safe
from django.utils.encoding import smart_unicode
import re

# coding: utf8
from .models import User
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

class PhoneField(forms.Field):
    def validate(self, value):
        super(PhoneField, self).validate(value)
        # Проверяем на соответствие поля телефонному номеру
        if re.compile("(?!:\A|\s)(?!(\d{1,6}\s+\D)|((\d{1,2}\s+){2,2}))(((\+\d{1,3})|(\(\+\d{1,3}\)))\s*)?((\d{1,6})|(\(\d{1,6}\)))\/?(([ -.]?)\d{1,5}){1,5}((\s*(#|x|(ext))\.?\s*)\d{1,5})?(?!:(\Z|\w|\b\s))").search(smart_unicode(value)):
            pass
        # Если не соответствует ничему, то вызываем ошибку
        else:
            raise forms.ValidationError(_(u'Введите правильный номер телефона.'), code='invalid')

class DivErrorList(ErrorList):
    def __unicode__(self):
        return self.as_divs()
    def as_divs(self):
        if not self: return u''
        return mark_safe(u'%s' % ''.join([u'<p class = "text-error">%s</p>' % e for e in self]))

# Допиливаем форму добавления пользователя. В Meta.model указываем нашу модель.
# Поля указывать нет необходимости т.к. они переопределяются в UserAdmin.add_fieldsets
class AdminUserAddForm(UserCreationForm):

    class Meta:
        model = User

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

# Допиливаем форму редактирования пользователя. В Meta.model указываем нашу модель.
class AdminUserChangeForm(UserChangeForm):

    phone=PhoneField(label=u"Телефон")
    class Meta:
        model = User

class NewUserForm(UserCreationForm):

    email = forms.EmailField(widget = forms.TextInput(attrs={"class":"email"}))
    username = forms.CharField(widget = forms.TextInput(attrs={"class":"email"}))
    confirm = forms.BooleanField()
    class Meta:
        model = User
        fields = ("username","phone","email")

class UserChangeForm(UserChangeForm):
    
    phone=PhoneField(label=u"Телефон")
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }
    class Meta:
        model = User
        fields = ("username", "email", "phone", "password")

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

class LoginForm(AuthenticationForm):
    captcha = CaptchaField()

class PasswordChangeForm(forms.ModelForm):
    """
    ModelForm for auth.User - used for signup and profile update.
    If a Profile model is defined via ``AUTH_PROFILE_MODULE``, its
    fields are injected into the form.
    """

    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput())
    password2 = forms.CharField(label=_("Password (again)"),
                                widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ()
    def __init__(self, *args, **kwargs):
        super(PasswordChangeForm, self).__init__(*args, **kwargs)
        self._signup = self.instance.id is None
        for field in self.fields:
            if field.startswith("password"):
                self.fields[field].widget.attrs["autocomplete"] = "off"
                self.fields[field].widget.attrs.pop("required", "")
                if not self._signup:
                    self.fields[field].required = False

    def clean_password2(self):
        """
        Ensure the password fields are equal, and match the minimum
        length defined by ``ACCOUNTS_MIN_PASSWORD_LENGTH``.
        """
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        errors = []
        if password1 != password2:
            errors.append(_("The two password fields didn't match."))
        if len(password1) < settings.ACCOUNTS_MIN_PASSWORD_LENGTH:
            errors.append(_(u"Пароль не должен быть короче %s символов") %
                         settings.ACCOUNTS_MIN_PASSWORD_LENGTH)
        if errors:
            self._errors["password1"] = self.error_class(errors)
        return password2

    def save(self, *args, **kwargs):
        """
        Create the new user using their email address as their username.
        """
        user = super(PasswordChangeForm, self).save(*args, **kwargs)
        password = self.cleaned_data.get("password1")
        if password:
            user.set_password(password)
            user.save()

        return user

class SendMailForm(forms.Form):
    name        = forms.CharField(max_length=30)
    email       = forms.EmailField()
    text        = forms.CharField(widget=forms.Textarea)
    captcha     = CaptchaField()
    copy_to_me  = forms.BooleanField(required = False)