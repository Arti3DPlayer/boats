# -*- coding: utf-8 -*-
from django.contrib.syndication.views import Feed
from boats.articles.models import Article, Category

class LatestArticles(Feed):
    title = "Новости и статьи на Proboating.ru"
    link = u"/articles/"
    description = "Описание"

    def items(self):
        return Article.objects.order_by('-pub_date')[:5]

    def item_link(self, item):
        return u"/articles/%s/%s/" % (Category.objects.get(id=item.category.id) ,item.slug)

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description