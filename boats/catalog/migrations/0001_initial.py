# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Country'
        db.create_table(u'catalog_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'catalog', ['Country'])

        # Adding model 'State'
        db.create_table(u'catalog_state', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Country'])),
        ))
        db.send_create_signal(u'catalog', ['State'])

        # Adding model 'WaterTransport'
        db.create_table(u'catalog_watertransport', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('slug', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('logo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
        ))
        db.send_create_signal(u'catalog', ['WaterTransport'])

        # Adding model 'TypeTransport'
        db.create_table(u'catalog_typetransport', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('kind', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.WaterTransport'])),
        ))
        db.send_create_signal(u'catalog', ['TypeTransport'])

        # Adding model 'Brand'
        db.create_table(u'catalog_brand', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('slug', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('alt_name', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('logo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
            ('company', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Country'])),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'catalog', ['Brand'])

        # Adding model 'FuelType'
        db.create_table(u'catalog_fueltype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'catalog', ['FuelType'])

        # Adding model 'BodyMaterial'
        db.create_table(u'catalog_bodymaterial', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'catalog', ['BodyMaterial'])

        # Adding model 'BodyShape'
        db.create_table(u'catalog_bodyshape', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'catalog', ['BodyShape'])

        # Adding model 'EngineNumber'
        db.create_table(u'catalog_enginenumber', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'catalog', ['EngineNumber'])

        # Adding model 'EngineType'
        db.create_table(u'catalog_enginetype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'catalog', ['EngineType'])

        # Adding model 'Currency'
        db.create_table(u'catalog_currency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'catalog', ['Currency'])

        # Adding model 'EnginePower'
        db.create_table(u'catalog_enginepower', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('power', self.gf('django.db.models.fields.FloatField')(default=0)),
        ))
        db.send_create_signal(u'catalog', ['EnginePower'])

        # Adding model 'Year'
        db.create_table(u'catalog_year', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('year', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'catalog', ['Year'])

        # Adding model 'Features'
        db.create_table(u'catalog_features', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('kind', self.gf('django.db.models.fields.related.ForeignKey')(related_name='product', null=True, to=orm['catalog.WaterTransport'])),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.TypeTransport'], null=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(related_name='product', null=True, to=orm['catalog.Brand'])),
            ('model', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
            ('year', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Year'], null=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Country'], null=True)),
            ('body_material', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.BodyMaterial'], null=True)),
            ('body_shape', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.BodyShape'], null=True)),
            ('engine_number', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.EngineNumber'], null=True)),
            ('engine_name', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
            ('engine_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.EngineType'], null=True)),
            ('engine_power', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.EnginePower'], null=True)),
            ('fuel_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.FuelType'], null=True)),
            ('length', self.gf('django.db.models.fields.FloatField')(default=0, null=True)),
            ('width', self.gf('django.db.models.fields.FloatField')(default=0, null=True)),
            ('draft', self.gf('django.db.models.fields.FloatField')(default=0, null=True)),
            ('height_above_water', self.gf('django.db.models.fields.FloatField')(default=0, null=True)),
            ('displacement', self.gf('django.db.models.fields.FloatField')(default=0, null=True)),
            ('weight', self.gf('django.db.models.fields.FloatField')(default=0, null=True)),
            ('cruising_speed', self.gf('django.db.models.fields.FloatField')(default=0, null=True)),
            ('max_speed', self.gf('django.db.models.fields.FloatField')(default=0, null=True)),
            ('fuel_capacity', self.gf('django.db.models.fields.FloatField')(default=0, null=True)),
            ('water_capacity', self.gf('django.db.models.fields.FloatField')(default=0, null=True)),
            ('passenger', self.gf('django.db.models.fields.IntegerField')(default=0, null=True)),
            ('cabins_number', self.gf('django.db.models.fields.IntegerField')(default=0, null=True)),
            ('beds_number', self.gf('django.db.models.fields.IntegerField')(default=0, null=True)),
            ('latrines_number', self.gf('django.db.models.fields.IntegerField')(default=0, null=True)),
        ))
        db.send_create_signal(u'catalog', ['Features'])

        # Adding model 'Product'
        db.create_table(u'catalog_product', (
            (u'features_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['catalog.Features'], unique=True, primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('slug', self.gf('django.db.models.fields.CharField')(unique=True, max_length=60)),
            ('text', self.gf('redactor.fields.RedactorField')()),
            ('preview', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
            ('rate', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'catalog', ['Product'])

        # Adding model 'Ad'
        db.create_table(u'catalog_ad', (
            (u'features_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['catalog.Features'], unique=True, primary_key=True)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('yacht_state', self.gf('django.db.models.fields.CharField')(max_length=1, null=True)),
            ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Currency'], null=True)),
            ('cost', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('contract_cost', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('user_country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Country'], null=True)),
            ('user_state', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.State'], null=True)),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, auto_now_add=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20, null=True)),
            ('note', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=30, null=True)),
            ('in_stock', self.gf('django.db.models.fields.CharField')(max_length=1, null=True)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounts.User'])),
        ))
        db.send_create_signal(u'catalog', ['Ad'])

        # Adding model 'ProductImage'
        db.create_table(u'catalog_productimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('upload_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(related_name='product_image', to=orm['catalog.Product'])),
        ))
        db.send_create_signal(u'catalog', ['ProductImage'])

        # Adding model 'AdImage'
        db.create_table(u'catalog_adimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('upload_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('ad', self.gf('django.db.models.fields.related.ForeignKey')(related_name='ad_image', to=orm['catalog.Ad'])),
        ))
        db.send_create_signal(u'catalog', ['AdImage'])


    def backwards(self, orm):
        # Deleting model 'Country'
        db.delete_table(u'catalog_country')

        # Deleting model 'State'
        db.delete_table(u'catalog_state')

        # Deleting model 'WaterTransport'
        db.delete_table(u'catalog_watertransport')

        # Deleting model 'TypeTransport'
        db.delete_table(u'catalog_typetransport')

        # Deleting model 'Brand'
        db.delete_table(u'catalog_brand')

        # Deleting model 'FuelType'
        db.delete_table(u'catalog_fueltype')

        # Deleting model 'BodyMaterial'
        db.delete_table(u'catalog_bodymaterial')

        # Deleting model 'BodyShape'
        db.delete_table(u'catalog_bodyshape')

        # Deleting model 'EngineNumber'
        db.delete_table(u'catalog_enginenumber')

        # Deleting model 'EngineType'
        db.delete_table(u'catalog_enginetype')

        # Deleting model 'Currency'
        db.delete_table(u'catalog_currency')

        # Deleting model 'EnginePower'
        db.delete_table(u'catalog_enginepower')

        # Deleting model 'Year'
        db.delete_table(u'catalog_year')

        # Deleting model 'Features'
        db.delete_table(u'catalog_features')

        # Deleting model 'Product'
        db.delete_table(u'catalog_product')

        # Deleting model 'Ad'
        db.delete_table(u'catalog_ad')

        # Deleting model 'ProductImage'
        db.delete_table(u'catalog_productimage')

        # Deleting model 'AdImage'
        db.delete_table(u'catalog_adimage')


    models = {
        u'accounts.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'catalog.ad': {
            'Meta': {'object_name': 'Ad', '_ormbases': [u'catalog.Features']},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.User']"}),
            'contract_cost': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cost': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Currency']", 'null': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True'}),
            u'features_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['catalog.Features']", 'unique': 'True', 'primary_key': 'True'}),
            'in_stock': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'user_country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Country']", 'null': 'True'}),
            'user_state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.State']", 'null': 'True'}),
            'yacht_state': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True'})
        },
        u'catalog.adimage': {
            'Meta': {'object_name': 'AdImage'},
            'ad': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ad_image'", 'to': u"orm['catalog.Ad']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'upload_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'catalog.bodymaterial': {
            'Meta': {'object_name': 'BodyMaterial'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'catalog.bodyshape': {
            'Meta': {'object_name': 'BodyShape'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'catalog.brand': {
            'Meta': {'object_name': 'Brand'},
            'alt_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Country']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'catalog.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'catalog.currency': {
            'Meta': {'object_name': 'Currency'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'catalog.enginenumber': {
            'Meta': {'object_name': 'EngineNumber'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'catalog.enginepower': {
            'Meta': {'object_name': 'EnginePower'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'power': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        u'catalog.enginetype': {
            'Meta': {'object_name': 'EngineType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'catalog.features': {
            'Meta': {'object_name': 'Features'},
            'beds_number': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'body_material': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.BodyMaterial']", 'null': 'True'}),
            'body_shape': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.BodyShape']", 'null': 'True'}),
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'product'", 'null': 'True', 'to': u"orm['catalog.Brand']"}),
            'cabins_number': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Country']", 'null': 'True'}),
            'cruising_speed': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True'}),
            'displacement': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True'}),
            'draft': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True'}),
            'engine_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'engine_number': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.EngineNumber']", 'null': 'True'}),
            'engine_power': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.EnginePower']", 'null': 'True'}),
            'engine_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.EngineType']", 'null': 'True'}),
            'fuel_capacity': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True'}),
            'fuel_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.FuelType']", 'null': 'True'}),
            'height_above_water': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kind': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'product'", 'null': 'True', 'to': u"orm['catalog.WaterTransport']"}),
            'latrines_number': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'length': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True'}),
            'max_speed': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'passenger': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.TypeTransport']", 'null': 'True'}),
            'water_capacity': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True'}),
            'weight': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True'}),
            'width': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True'}),
            'year': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Year']", 'null': 'True'})
        },
        u'catalog.fueltype': {
            'Meta': {'object_name': 'FuelType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'catalog.product': {
            'Meta': {'object_name': 'Product', '_ormbases': [u'catalog.Features']},
            u'features_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['catalog.Features']", 'unique': 'True', 'primary_key': 'True'}),
            'preview': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'rate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'text': ('redactor.fields.RedactorField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'catalog.productimage': {
            'Meta': {'object_name': 'ProductImage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'product_image'", 'to': u"orm['catalog.Product']"}),
            'upload_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'catalog.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'catalog.typetransport': {
            'Meta': {'object_name': 'TypeTransport'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kind': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.WaterTransport']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'catalog.watertransport': {
            'Meta': {'object_name': 'WaterTransport'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'catalog.year': {
            'Meta': {'object_name': 'Year'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['catalog']