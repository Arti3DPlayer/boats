# -*- coding: utf-8 -*-
from django.conf import settings
from django import template
from django.utils.translation import ugettext_lazy as _
from django.contrib.flatpages.models import FlatPage
from django.db.models import Q, F, Avg, Count

from boats.catalog import models
from boats.catalog import forms
from boats.catalog.views import filter_list

register = template.Library()

@register.inclusion_tag('catalog/inclusion/footer_watertransport_list.html')
def watertrasport_footer_list_tag():
    w = models.WaterTransport.objects.all()
    return {
        'watertransport': w
        }

@register.inclusion_tag('catalog/inclusion/mini_brands_list.html')
def mini_brands_list_tag():
    b = models.Brand.objects.all()[:29]
    return {
        'brands': b
        }

@register.inclusion_tag('catalog/inclusion/popular_transport_list.html')
def popular_transport_list_tag():
    p = models.Product.objects.filter(rate=True)
    return {
        'products': p
        }

@register.inclusion_tag('catalog/inclusion/catalog_search_form.html', takes_context=True)
def catalog_search_form_tag(context):
	request = context['request']
	boat_form = forms.CatalogBoatSearchForm()
	model_form = forms.CatalogModelSearchForm()

	if request.session.has_key('boat_form_errors'):
		errors = request.session.pop('boat_form_errors')
		return {
        	'CatalogBoatSearchForm': boat_form,
        	'CatalogModelSearchForm': model_form,
        	'CatalogBoatSearchFormErrors': errors,
        }

	return {
		'CatalogBoatSearchForm': boat_form,
		'CatalogModelSearchForm': model_form,
		}