# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.forms import ModelForm, Textarea ,TextInput, PasswordInput , ModelChoiceField , ChoiceField
from boats.catalog.models import Ad, Product, WaterTransport, TypeTransport, Brand, FuelType, Country, State, Year, BodyMaterial, BodyShape, EngineNumber, EngineType, EnginePower, Currency, AdPhone
from captcha.fields import CaptchaField
from django.forms.models import inlineformset_factory
from boats.accounts.models import User

KIND_CHOICES = (WaterTransport.objects.all().values_list('id', 'name'))
TYPE_CHOICES = (TypeTransport.objects.all().values_list('id', 'name'))
BRAND_CHOICES = (Brand.objects.all().values_list('id', 'name'))
FUEL_TYPE_CHOICES = (FuelType.objects.all().values_list('id', 'name'))
COUNTRY_CHOICES = (Country.objects.all().values_list('id', 'name'))
STATE_CHOICES = (State.objects.all().values_list('id', 'name'))
YEAR_CHOICES = (Year.objects.all().values_list('id', 'year'))
CURRENCY_CHOICES = (Currency.objects.all().values_list('id', 'name'))
BODY_MATERIAL_CHOISES = (BodyMaterial.objects.all().values_list('id', 'name'))
BODY_SHAPE_CHOISES = (BodyShape.objects.all().values_list('id', 'name'))
ENGINE_NUMBER_CHOISES = (EngineNumber.objects.all().values_list('id', 'number'))
ENGINE_TYPE_CHOISES = (EngineType.objects.all().values_list('id', 'name'))
ENGINE_POWER_CHOISES = (EnginePower.objects.all().values_list('id', 'power'))
PRODUCT_CHOICES = (Product.objects.all().values_list('id', 'title'))
AD_CHOICES = (Ad.objects.all().values_list('id', 'model'))
YACHT_STATE_CHOICES = (
        ('1', 'Новое'),
        ('2', 'Б/У'),
    )

IN_STOCK_CHOICES = (
        ('1', 'В наличии'),
        ('2', 'В пути'),
        ('3', 'Под заказ'),
    )

from itertools import chain
from django.forms import RadioSelect
from django.utils.encoding import force_unicode

class RadioSelectNotNull(RadioSelect):
    def get_renderer(self, name, value, attrs=None, choices=()):
        """Returns an instance of the renderer."""
        if value is None: value = ''
        str_value = force_unicode(value) # Normalize to string.
        final_attrs = self.build_attrs(attrs)
        choices = list(chain(self.choices, choices))
        if choices[0][0] == '':
            choices.pop(0)
        return self.renderer(name, str_value, final_attrs, choices)



class AdAddForm(forms.ModelForm):
    def __init__(self, author, *args, **kwargs):
        self.author = author
        super(AdAddForm, self).__init__(*args, **kwargs)
        self.fields['kind'].empty_label = None
        self.fields['in_stock'].empty_label = None

    length_ft = forms.FloatField(label="Длинна(футы)")
    width_ft  = forms.FloatField(label="Ширина(футы)")
    captcha   = CaptchaField(label="Каптча")
    contract_cost = forms.BooleanField(label="Цена",required=False)

    def save(self):
        form = super(AdAddForm, self).save(commit= False)
        cd = self.cleaned_data
        form.author = self.author
        form.active = True
        if cd['contract_cost']:
            form.cost=0
        form.save()
        return form

    def clean(self):
        cleaned_data = super(AdAddForm, self).clean()
        cost = cleaned_data.get('cost')
        currency = cleaned_data.get('currency')
        contract_cost = cleaned_data.get('contract_cost')

        if not(cost and currency) and contract_cost==False:
            raise forms.ValidationError("Введите цену или выберите договорную.")

        return cleaned_data

    class Meta:
        model = Ad
        widgets = {
            'kind': forms.RadioSelect(),
            'in_stock': RadioSelectNotNull(),
            'yacht_state': RadioSelectNotNull(),
        }
        exclude = ('author','views')

class AdAddPhoneForm(forms.ModelForm):
    class Meta:
        model = AdPhone

AdAddPhoneFormSet = inlineformset_factory(Ad, AdPhone, extra=1, max_num=10, can_delete=False)

class CatalogBoatSearchForm(forms.Form):
    boat_brand = forms.ChoiceField(widget=forms.Select(attrs={}), choices=BRAND_CHOICES)
    boat_length_from = forms.FloatField(label="from", required=False, widget = forms.TextInput(attrs={} ))
    boat_length_to = forms.FloatField(label="to", required=False, widget = forms.TextInput(attrs={} ))
    boat_kind = forms.ChoiceField(widget=forms.RadioSelect(attrs={}), required=False, choices=([('',u'Все')]+list(WaterTransport.objects.all().values_list('id', 'name'))))
    boat_year_from = forms.IntegerField(label="from", required=False, widget = forms.TextInput())
    boat_year_to = forms.IntegerField(label="to", required=False, widget = forms.TextInput())
    boat_cost_from = forms.FloatField(label="from", required=False, widget = forms.TextInput())
    boat_cost_to = forms.FloatField(label="to", required=False, widget = forms.TextInput())

class CatalogModelSearchForm(forms.Form):
    model_brand = forms.ChoiceField(widget=forms.Select(attrs={}), choices=BRAND_CHOICES)
    model_product = forms.ChoiceField(widget=forms.Select(attrs={}), choices=PRODUCT_CHOICES)


class CatalogAdSearchForm(forms.Form):
    country = forms.ChoiceField(widget=forms.Select(attrs={"class":"select200px"}), choices=COUNTRY_CHOICES)
    state = forms.ChoiceField(widget=forms.Select(attrs={"class":"select200px"}), choices=STATE_CHOICES)
    brand = forms.ChoiceField(widget=forms.Select(attrs={"class":"select200px"}), choices=([('',u'Все')]+list(Brand.objects.all().values_list('id', 'name'))))
    ad = forms.ChoiceField(widget=forms.Select(attrs={}), choices=AD_CHOICES)
    length_from = forms.FloatField(label="from", required=False, widget = forms.TextInput(attrs={ }))
    length_to = forms.FloatField(label="to", required=False, widget = forms.TextInput(attrs={"class":"pull-right"} ))
    year_from = forms.IntegerField(label="from", required=False, widget = forms.TextInput(attrs={ }))
    year_to = forms.IntegerField(label="to", required=False, widget = forms.TextInput(attrs={"class":"pull-right"} ))
    cost_from = forms.FloatField(label="from", required=False, widget = forms.TextInput(attrs={} ))
    cost_to = forms.FloatField(label="to", required=False, widget = forms.TextInput(attrs={} ))
    currency = forms.ChoiceField(widget=forms.Select(attrs={"class":"select76px"}), choices=CURRENCY_CHOICES)