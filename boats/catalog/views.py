# -*- coding: utf-8 -*-
from .. decorators import render_to, render_to_json
from django.views.decorators.csrf import csrf_exempt

from django.template import RequestContext
from django.http import Http404,HttpResponse
from django.shortcuts import render_to_response, HttpResponseRedirect, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.core import serializers
from django.db.models import Q
from boats.catalog.models import Brand, WaterTransport, Product, TypeTransport, Ad, Year, BodyMaterial, EngineType, FuelType, BodyShape, EnginePower, Country, State, Currency, EngineNumber,AdImage, AdPhone
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.utils import simplejson
from sorl.thumbnail import get_thumbnail
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from boats.catalog.forms import AdAddForm, CatalogBoatSearchForm, CatalogModelSearchForm, CatalogAdSearchForm, AdAddPhoneFormSet
from boats.articles.models import Article, Category
from django.core.files.uploadedfile import UploadedFile
import itertools
from boats.accounts.forms import SendMailForm
from django.contrib.flatpages.models import FlatPage

@render_to_json
def admin_type_select(request):
    """
    Связные списки типа и вида транспорта в админке
    """
    data = {}
    if request.GET.get('watertransport'):
        kind_id = request.GET.get('watertransport')
        data = TypeTransport.objects.filter(kind=kind_id)
    data=serializers.serialize('json',data)
    return data

@render_to_json
def country_select_ajax(request):
    """
    Связные списки страны и города
    """
    data = {}
    if request.GET.get('country'):
        country_id = request.GET.get('country')
        data = State.objects.filter(country=country_id)
    data=serializers.serialize('json',data)
    return data


@render_to('catalog/inclusion/model_form_ajax.html')
def search_model_ajax(request):
    """
    Связные списки для поиска по модели
    """
    if request.GET.get('brand'):
        brand_id = request.GET.get('brand')
        p = Product.objects.filter(brand_id=brand_id)
    return {
        'products':p,
    }

@render_to('catalog/catalog_list.html')
def catalog_list(request):
    brands = Brand.objects.all()
    water_transport = WaterTransport.objects.all()
    return {
        'brands': brands,
        'water_transport': water_transport,
    }

@render_to('catalog/brand_list.html')
def brand_list(request,slug):
    w = get_object_or_404(WaterTransport, slug=slug)
    random_water_transport = WaterTransport.objects.order_by('?')[:2]
    b = Brand.objects.all()
    return {
        'brands': b,
        'water_transport': w,
        'random_water_transport':random_water_transport,
    }

@render_to('catalog/brand_all.html')
def brand_all(request):
    b = Brand.objects.all()
    return {
        'brands': b
    }

@render_to('catalog/brand_details.html')
def brand_details(request,slug):
    b = get_object_or_404(Brand, slug=slug)
    p= dict()
    for k,v in itertools.groupby(Product.objects.filter(brand=b), lambda obj: obj.type):
        p[k] = list(v)
    return {
        'brand': b,
        'product': p,
    }

@render_to('catalog/product_details.html')
def product_details(request,slug):
    p = get_object_or_404(Product, slug=slug)
    a = Article.objects.filter(tags__icontains= p.brand)[:24]
    s = Ad.objects.filter(kind=p.kind)[:24]
    return {
        'product': p,
        'same_articles': a,
        'same_ads': s,
    }


@render_to('catalog/boat_search_result.html')
def boats_search(request):
    """Поиск по каталогу по критериям: Марка, Длинна от, Длинна до, Вид транспорта.
    Возвращает itertools обьект где ключ это вид транспорта, а значения - продукты этого вида.
    При не удачной валидации записывается сессия и проверяется в темплейт теге.
    """
    if request.method=='POST':
        if 'boat-search' in request.POST:
            boat_form = CatalogBoatSearchForm(request.POST)
            if boat_form.is_valid():
                cd = boat_form.cleaned_data
                q_list = list()
                if cd['boat_brand']:
                    q_list.append(Q(brand__id= cd['boat_brand']))
                if cd['boat_kind']:
                    q_list.append(Q(kind__id= cd['boat_kind']))
                if cd['boat_length_from']:
                    q_list.append(Q(length__gte= cd['boat_length_from']))
                if cd['boat_length_to']:
                    q_list.append(Q(length__lte= cd['boat_length_to']))
                if cd['boat_year_from']:
                    q_list.append(Q(year__gte= cd['boat_year_from']))
                if cd['boat_year_to']:
                    q_list.append(Q(year__lte= cd['boat_year_to']))
                if cd['boat_cost_from']:
                    q_list.append(Q(cost__gte= cd['boat_cost_from']))
                if cd['boat_cost_to']:
                    q_list.append(Q(cost__lte= cd['boat_cost_to']))

                p= dict()
                for k,v in itertools.groupby(Product.objects.filter(*q_list), lambda obj: obj.type):
                    p[k] = list(v)

                return {
                    'search_choose':cd,
                    'product': p,
                }
            request.session['boat_form_errors'] = boat_form.errors
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def filter_list(request):
    """Возвращает список фильтров находящихся в сессии"""
    filters = {
        'kind': 'kind__id',
        'yacht_state': 'yacht_state',
        'id_country': 'user_country',
        'id_state': 'user_state',
        'id_brand': 'brand__id',
        'id_model_product': 'model',
    }

    query_list = []
    filter_list = {}
    for filter_name, filter_attr in filters.items():
        if request.session.has_key(filter_name):
            query_list.append(Q(**{filter_attr: request.session.get(filter_name)[0]}))
            filter_list[filter_name] = request.session.get(filter_name)[1]

    return query_list, filter_list

@csrf_exempt
def ads_add_filter(request):
    if request.method == 'POST':
        id = request.POST['id']
        name = request.POST['name']
        type = request.POST['type']
        request.session[type] = [id,name]
    return HttpResponse("OK")

def ads_del_filter(request,type):
    del request.session[type]
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@render_to('catalog/inclusion/catalog_ad_filter_list.html')
def ads_block_filters(request):
    filters = filter_list(request)[1]

    return {
        'filters': filters,
    }


@render_to('catalog/ads_block_list.html')
def ads_block_list(request):
    filters = filter_list(request)[0]
    ads = Ad.objects.filter(*filters,active=True).order_by('-pub_date')
    ads_count = ads.count()
    try:
        num_ads = int(request.session['ads_list_ads_on_page'])
    except:
        num_ads = 10

    ads_on_page = request.GET.get('ads_on_page', num_ads)
    request.session['ads_list_ads_on_page'] = ads_on_page

    paginator = Paginator(ads, ads_on_page)
    page = request.GET.get('page')
    try:
        ads = paginator.page(page)
    except PageNotAnInteger:
        ads = paginator.page(1)
    except EmptyPage:
        ads = paginator.page(paginator.num_pages)

    if request.is_ajax():    
        return {
            'ads': ads,
            'ads_count': ads_count,
        }
    else:
        return HttpResponseRedirect("/catalog/ads/")

@render_to('catalog/ads_list.html')
def ads_list(request):
    w = WaterTransport.objects.all()
    f = CatalogAdSearchForm()

    return {
        'watertransport': w,
        'CatalogAdSearchForm': f,
    }

@render_to('catalog/ads_detail.html')
def ads_detail(request,id):
    ad = get_object_or_404(Ad, id=id)
    ad.views+=1
    ad.save()
    a = Article.objects.filter(tags__icontains= ad.brand)[:24]
    s = Ad.objects.exclude(id=ad.id).filter(kind=ad.kind)[:24]
    return {
        'ad': ad,
        'send_mail_form': SendMailForm(),
        'same_articles': a,
        'same_ads': s,
    }


from django.utils.encoding import smart_str
from django.contrib import messages

@login_required(login_url = "/accounts/about/")
@render_to('catalog/ad_edit.html')
def ad_edit(request, id):
    ad = get_object_or_404(Ad, id=id, author=request.user)
    if request.method == 'POST':
        if request.is_ajax():
            if request.FILES == None:
                return HttpResponseBadRequest('Must have files attached!')

            file = request.FILES[u'files[]']
            wrapped_file = UploadedFile(file)
            filename = wrapped_file.name
            file_size = wrapped_file.file.size

            #obj = get_object_or_404(Ad, id=id)
            image = AdImage()
            image.image=file
            image.ad = ad
            image.author = request.user
            image.save()
            #getting url for photo deletion
            file_delete_url = '/catalog/ad/image/delete/'
            #getting file url here
            file_url = '/'

            im = get_thumbnail(image, "180x120", quality=50)
            thumb_url = im.url

            result = []
            result.append({"name":filename, 
                        "size":file_size, 
                        "url":file_url, 
                        "thumbnail_url":thumb_url,
                        "delete_url":file_delete_url+str(image.pk)+'/',
                        "pk": str(image.pk), 
                        "delete_type":"GET",})
            response_data = simplejson.dumps(result)
            return HttpResponse(response_data, mimetype='application/json')
        else:
            f = AdAddForm(instance=ad,data=request.POST or None, author= request.user)
            phoneformset = AdAddPhoneFormSet(instance=ad,data=request.POST or None)
            if f.is_valid() and phoneformset.is_valid():
                images = AdImage.objects.filter(ad=ad).update(preview=False)
                try:
                    image = AdImage.objects.get(pk=request.POST.get('image-preview', 0))
                    image.preview=True
                    image.save()
                except AdImage.DoesNotExist:
                    if AdImage.objects.filter(ad=ad).count() > 0: 
                        image = AdImage.objects.filter(ad=ad)[0]
                        image.preview=True
                        image.save()
                obj = f.save()
                phoneformset.save()
                return HttpResponseRedirect('/accounts/my_ads/')
    else:
        f = AdAddForm(instance=ad, author= request.user)
        phoneformset = AdAddPhoneFormSet(instance=ad)
    return {
        'AdAddForm':f,
        'PhoneFormSet': phoneformset,
        'ad': ad,
    }

def ad_add(request):
    """Создаём не активный обьект и перенаправляем на страницу редактирования"""
    """Сделано для мультизагрузки изображений и их привязки к обьекту"""

    ad = Ad.objects.create(author=request.user, views=0)
    return HttpResponseRedirect('/catalog/ads/edit/'+str(ad.id)+'/')


def adimage_delete(request, pk):
    image = get_object_or_404(AdImage, pk=pk)
    if image.author == request.user:
        image.delete()
    else:
        print "Access denied"
    return HttpResponse(str(pk))

@render_to('flatpages/howto_by.html')
def howto_buy(request):
    try:
        block_decr = FlatPage.objects.get(url = '/howto_buy_or_sell/')
        block_decr_buy = FlatPage.objects.get(url = '/howto_buy/')
        block_decr_sell = FlatPage.objects.get(url = '/howto_sell/')
    except FlatPage.DoesNotExist:
        raise Http404
    return{
        'buy_or_sell': block_decr,
        'buy': block_decr_buy,
        'sell': block_decr_sell,
    }