# -*- coding: utf-8 -*-
from django.contrib import admin
from boats.catalog.models import WaterTransport, TypeTransport, Brand, Country, State, FuelType, Product, Ad, BodyMaterial, BodyShape, EngineNumber, EngineType, EnginePower, Year, ProductImage, AdImage, Currency, AdPhone
from django import forms
from redactor.widgets import RedactorEditor

class StateInline(admin.TabularInline):
    model = State

class CountryAdmin(admin.ModelAdmin):
    list_display = ('name',)
    inlines = [StateInline,]
    
class TypeTransportInline(admin.TabularInline):
    model = TypeTransport

class WaterTransportAdmin(admin.ModelAdmin):
    list_display = ('name',)
    inlines = [TypeTransportInline,]

class BrandAdmin(admin.ModelAdmin):
    list_display = ('name',)

class BodyMaterialAdmin(admin.ModelAdmin):
    list_display = ('name',)

class BodyShapeAdmin(admin.ModelAdmin):
    list_display = ('name',)

class FuelTypeAdmin(admin.ModelAdmin):
    list_display = ('name',)

class EngineNumberAdmin(admin.ModelAdmin):
    list_display = ('number',)

class EngineTypeAdmin(admin.ModelAdmin):
    list_display = ('name',)

class EnginePowerAdmin(admin.ModelAdmin):
    list_display = ('power',)

class YearAdmin(admin.ModelAdmin):
    list_display = ('year',)

class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('name',)

class ProductImageInline(admin.TabularInline):
    model = ProductImage

class AdImageInline(admin.TabularInline):
    model = AdImage
    exclude = ('author',)
    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()

class AdPhoneInline(admin.TabularInline):
    model = AdPhone
    extra = 1

class ProductAdmin(admin.ModelAdmin):
    list_display = ('title','kind','type','pub_date', 'rate')
    list_filter = ('pub_date',)
    search_fields = ('title', 'kind', 'type',)
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'text', 'preview', 'rate')
        }),
        (u'Основные характеристики', {
            'classes': ('wide',),
            'fields': ('kind', 'type', 'brand', 'model', 'year', 'country', 'body_material', 'body_shape', 'engine_number', 'engine_name', 'engine_type', 'engine_power', 'fuel_type', 'length', 'width')
        }),
        (u'Не обязательные характеристики', {
            'classes': ('collapse',),
            'fields': ('draft','height_above_water','displacement','weight','cruising_speed','max_speed', 'fuel_capacity', 'water_capacity', 'passenger', 'cabins_number', 'beds_number', 'latrines_number')
        }),
    )
    inlines = [ProductImageInline,]

class AdAdmin(admin.ModelAdmin):
    list_display = ('model','kind','type','pub_date', 'active', 'author')
    list_filter = ('pub_date',)
    search_fields = ('model','kind','type','pub_date', 'active', 'author')
    fieldsets = (
        (None, {
            'fields': ('text', 'yacht_state', 'cost','currency', 'email', 'in_stock','user_country','user_state','active', 'note')
        }),
        (u'Основные характеристики', {
            'classes': ('wide',),
            'fields': ('kind', 'type', 'brand', 'model', 'year', 'country', 'body_material', 'body_shape', 'engine_number', 'engine_name', 'engine_type', 'engine_power', 'fuel_type', 'length', 'width')
        }),
        (u'Не обязательные характеристики', {
            'classes': ('collapse',),
            'fields': ('draft','height_above_water','displacement','weight','cruising_speed','max_speed', 'fuel_capacity', 'water_capacity', 'passenger', 'cabins_number', 'beds_number', 'latrines_number')
        }),
    )
    inlines = [AdPhoneInline, AdImageInline]

    def queryset (self,request):
        qs = super(AdAdmin,self).queryset(request)
        if request.user.is_superuser:
            return qs.filter(active=True)

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()


admin.site.register(WaterTransport,WaterTransportAdmin)
admin.site.register(Brand,BrandAdmin)
admin.site.register(Country,CountryAdmin)
admin.site.register(FuelType,FuelTypeAdmin)
admin.site.register(BodyMaterial,BodyMaterialAdmin)
admin.site.register(BodyShape,BodyShapeAdmin)
admin.site.register(EngineNumber,EngineNumberAdmin)
admin.site.register(EnginePower,EnginePowerAdmin)
admin.site.register(EngineType,EngineTypeAdmin)
admin.site.register(Year,YearAdmin)
admin.site.register(Currency,CurrencyAdmin)
admin.site.register(Product,ProductAdmin)
admin.site.register(Ad,AdAdmin)