# -*- coding: utf-8 -*-
from django.db import models
from redactor.fields import RedactorField
from boats.accounts.models import User


class Country(models.Model):
    name = models.CharField(max_length=30, verbose_name=u'Страна')

    class Meta:
        verbose_name = u'Страна'
        verbose_name_plural = u'Список стран'

    def __unicode__(self):
        return self.name

class State(models.Model):
    name    = models.CharField(max_length=30, verbose_name=u'Город')
    country = models.ForeignKey(Country)

    class Meta:
        verbose_name = u'Город'
        verbose_name_plural = u'Список городов'

    def __unicode__(self):
        return self.name

class WaterTransport(models.Model):
    name = models.CharField(max_length=30, verbose_name=u'Название')
    slug  = models.CharField(max_length=30, unique=True, verbose_name=u'Название категории [транслит]', help_text=u'Будет использоваться в url. Например: travel')
    description = models.TextField(verbose_name=u'Описание')
    logo = models.ImageField(verbose_name=u'Логотип', upload_to='water_transport/', null = True)

    @property  
    def get_brands(self):
        """возвращает все марки которые производят данный вид водного транспорта"""
        brands = list()
        for brand in Brand.objects.filter(product__kind__name = self.name)[:6]:
            brands.append(brand)
        return set(brands)

    class Meta:
        verbose_name = u'Вид водного транспорта'
        verbose_name_plural = u'Список видов водного транспорта'

    def __unicode__(self):
        return self.name

class TypeTransport(models.Model):
    name = models.CharField(max_length=30, verbose_name=u'Название')
    kind = models.ForeignKey(WaterTransport)
    class Meta:
        verbose_name = u'Тип водного транспорта'
        verbose_name_plural = u'Список типов водного транспорта'

    def __unicode__(self):
        return self.name


class Brand(models.Model):
    name = models.CharField(max_length=30, verbose_name=u'Название')
    slug  = models.CharField(max_length=30, unique=True, verbose_name=u'Название категории [транслит]', help_text=u'Будет использоваться в url. Например: travel')
    alt_name = models.CharField(max_length=200, blank=True, verbose_name=u'Альтернативные названия', help_text=u'Не обязательно')
    logo = models.ImageField(verbose_name=u'Логотип', upload_to='brands/', null = True)
    company = models.CharField(max_length=30, verbose_name=u'Компания владелец')
    country = models.ForeignKey(Country, verbose_name=u'Страна')
    description = models.TextField(verbose_name=u'Описание')

    @property  
    def get_products(self):
        return Product.objects.filter(brand = self)

    class Meta:
        verbose_name = u'Марка'
        verbose_name_plural = u'Список марок катеров'

    def __unicode__(self):
        return self.name


class FuelType(models.Model):
    name = models.CharField(max_length=30, verbose_name=u'Топливо')

    class Meta:
        verbose_name = u'Топливо'
        verbose_name_plural = u'Список видов топлива'

    def __unicode__(self):
        return self.name

class BodyMaterial(models.Model):
    name = models.CharField(max_length=30, verbose_name=u'Материал')

    class Meta:
        verbose_name = u'Материал корпуса'
        verbose_name_plural = u'Список материалов корпуса'

    def __unicode__(self):
        return self.name

class BodyShape(models.Model):
    name = models.CharField(max_length=30, verbose_name=u'Форма корпуса')

    class Meta:
        verbose_name = u'Форма корпуса'
        verbose_name_plural = u'Список форм корпуса'

    def __unicode__(self):
        return self.name

class EngineNumber(models.Model):
    number = models.IntegerField(default=0, verbose_name=u'Количество двигателей')

    class Meta:
        verbose_name = u'Количество двигателей'
        verbose_name_plural = u'Список количества двигателей'

    def __unicode__(self):
        return str(self.number).decode()

class EngineType(models.Model):
    name = models.CharField(max_length=30, verbose_name=u'Тип двигателя')

    class Meta:
        verbose_name = u'Тип двигателя'
        verbose_name_plural = u'Список типов двигателя'

    def __unicode__(self):
        return self.name

class Currency(models.Model):
    name = models.CharField(max_length=30, verbose_name=u'Валюта')

    class Meta:
        verbose_name = u'Валюта'
        verbose_name_plural = u'Список валют'

    def __unicode__(self):
        return self.name

class EnginePower(models.Model):
    power = models.FloatField(default=0, verbose_name=u'Мощность двигателя')

    class Meta:
        verbose_name = u'Мощность двигателя'
        verbose_name_plural = u'Список мощностей двигателя'

    def __unicode__(self):
        return str(self.power).decode()

class Year(models.Model):
    year = models.IntegerField(default=0, verbose_name=u'Год')

    class Meta:
        verbose_name = u'Год выпуска'
        verbose_name_plural = u'Список годов выпуска'

    def __unicode__(self):
        return str(self.year).decode()


class Features(models.Model):
    kind               = models.ForeignKey(WaterTransport, null=True, related_name="product", verbose_name=u'Вид водного транспорта')
    type               = models.ForeignKey(TypeTransport, null=True, verbose_name=u'Тип водного транспорта')
    brand              = models.ForeignKey(Brand, null=True, related_name="product", verbose_name=u'Марка')
    model              = models.CharField(max_length=200, null=True, verbose_name=u'Модель')
    year               = models.ForeignKey(Year, null=True, verbose_name=u'Год выпуска')
    country            = models.ForeignKey(Country,  blank=True, null=True, verbose_name=u'Страна-производитель')
    body_material      = models.ForeignKey(BodyMaterial,  blank=True, null=True, verbose_name=u'Материал корпуса')
    body_shape         = models.ForeignKey(BodyShape,  blank=True, null=True, verbose_name=u'Форма корпуса')
    engine_number      = models.ForeignKey(EngineNumber,  blank=True, null=True, verbose_name=u'Количество двигателей')
    engine_name        = models.CharField(max_length=200,  blank=True, null=True, verbose_name=u'Название двигателя')
    engine_type        = models.ForeignKey(EngineType,  blank=True, null=True, verbose_name=u'Тип двигателя')
    engine_power       = models.ForeignKey(EnginePower,  blank=True, null=True, verbose_name=u'Мощность двигателя')
    fuel_type          = models.ForeignKey(FuelType,  blank=True, null=True, verbose_name=u'Топливо')
    length             = models.FloatField(default=0, null=True, verbose_name=u'Длина', help_text="метров")
    width              = models.FloatField(default=0, null=True, verbose_name=u'Ширина', help_text="метров")
    draft              = models.FloatField(default=0,  blank=True, null=True, verbose_name=u'Осадка', help_text="метров")
    height_above_water = models.FloatField(default=0,  blank=True, null=True, verbose_name=u'Высота над уровнем воды', help_text="метров")
    displacement       = models.FloatField(default=0,  blank=True, null=True, verbose_name=u'Водоизмещение', help_text="кг")
    weight             = models.FloatField(default=0,  blank=True, null=True, verbose_name=u'Вес', help_text="кг")
    cruising_speed     = models.FloatField(default=0,  blank=True, null=True, verbose_name=u'Крейсерская скорость', help_text="узлов")
    max_speed          = models.FloatField(default=0,  blank=True, null=True, verbose_name=u'Максимальная скорость', help_text="узлов")
    fuel_capacity      = models.FloatField(default=0,  blank=True, null=True, verbose_name=u'Запас топлива', help_text="л")
    water_capacity     = models.FloatField(default=0,   blank=True, null=True, verbose_name=u'Запас воды', help_text="л")
    passenger          = models.IntegerField(default=0,  blank=True, null=True, verbose_name=u'Пассажировместимость', help_text="человек")
    cabins_number      = models.IntegerField(default=0,  blank=True, null=True, verbose_name=u'Количество кают')
    beds_number        = models.IntegerField(default=0,  blank=True, null=True, verbose_name=u'Спальных мест', help_text="человек")
    latrines_number    = models.IntegerField(default=0,  blank=True, null=True, verbose_name=u'Количество гальюнов')

    class Meta:
        verbose_name = u'Характеристики'
        verbose_name_plural = u'Характеристики'


class Product(Features):
    title = models.CharField(max_length=60, verbose_name=u'Название')
    slug  = models.CharField(max_length=60, unique=True, verbose_name=u'Название [транслит]', help_text=u'Будет использоваться в url. Например: travel')
    text = RedactorField(verbose_name=u'Описание катера')
    preview = models.ImageField(verbose_name=u'Логотип', upload_to='product_images/', null = True)
    rate = models.BooleanField(default=False, verbose_name=u'Популярная модель')
    pub_date = models.DateTimeField(auto_now=True, auto_now_add=True, verbose_name=u'Дата публикации')

    @property  
    def get_images(self):
        return ProductImage.objects.filter(product = self)

    class Meta:
        verbose_name = u'Продукт'
        verbose_name_plural = u'Продукты'

    def __unicode__(self):
        return self.title


class Ad(Features):
    YACHT_STATE_CHOICES = (
        ('1', 'Новое'),
        ('2', 'Б/У'),
    )
    IN_STOCK_CHOICES = (
        ('1', 'В наличии'),
        ('2', 'В пути'),
        ('3', 'Под заказ'),
    )
    text = models.TextField(verbose_name=u'Описание катера', null=True, blank=True)
    active = models.BooleanField(default=False, verbose_name=u'Показывать обьявление')
    yacht_state = models.CharField(max_length=1, null=True, verbose_name=u'Состояние', choices=YACHT_STATE_CHOICES)
    currency = models.ForeignKey(Currency, null=True, blank=True, verbose_name=u'Валюта')
    cost = models.FloatField(default=0, verbose_name=u'Стоимость', blank=True)
    user_country = models.ForeignKey(Country, null=True, verbose_name=u'Страна')
    user_state = models.ForeignKey(State, null=True, verbose_name=u'Город')
    pub_date = models.DateTimeField(auto_now=True, auto_now_add=True, verbose_name=u'Дата публикации')
    note = models.TextField(null=True, blank=True,verbose_name=u'Примечание')
    author = models.ForeignKey(User, null=True, blank=True, verbose_name=u'Автор')
    email = models.CharField(max_length=30, blank=True, null=True, verbose_name=u'E-mail')
    in_stock = models.CharField(max_length=1, blank=True, null=True, verbose_name=u'Наличие в городе продажи', choices=IN_STOCK_CHOICES)
    views = models.IntegerField(default=0, verbose_name=u'Просмотров')
    author = models.ForeignKey(User)

    @property
    def get_images(self):
        return AdImage.objects.filter(ad=self)

    @property
    def get_phones(self):
        return AdPhone.objects.filter(ad=self)

    @property
    def get_preview_image(self):
        return self.ad_image.get(preview= True)

    @property
    def show_cost(self):
        if(self.cost==0):
            return u"Договорная"
        else:
            return u"%s %s" % (self.cost, self.currency)

    @property
    def show_yacht_state(self):
        return self.get_yacht_state_display()
    class Meta:
        verbose_name = u'Обьявление'
        verbose_name_plural = u'Обьявления'


class ProductImage(models.Model):
    image = models.FileField(upload_to='product_images/')
    upload_date = models.DateTimeField(auto_now_add=True)
    product = models.ForeignKey(Product, related_name='product_image')

    class Meta:
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'

    def __unicode__(self):
        return self.image.name


class AdPhone(models.Model):
    number = models.CharField(max_length=20, null=True, verbose_name=u'Телефон')
    ad = models.ForeignKey(Ad)

    class Meta:
        verbose_name = u'Телефон'
        verbose_name_plural = u'Телефоны'

class AdImage(models.Model):
    preview = models.BooleanField(default=False, verbose_name=u'Превью')
    image = models.FileField(upload_to='ad_images/')
    upload_date = models.DateTimeField(auto_now_add=True)
    ad = models.ForeignKey(Ad, related_name='ad_image')
    author = models.ForeignKey(User)

    class Meta:
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'

    def __unicode__(self):
        return self.image.name

