# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings



urlpatterns = patterns('boats.catalog.views',
    url(r'^$', 'catalog_list', name='catalog_list'),
    url(r'brand/details/(?P<slug>\w+)/$', 'brand_details', name='brand_details'),
    url(r'brand/list/(?P<slug>\w+)/$', 'brand_list', name='brand_list'),
    url(r'brand/all/$', 'brand_all', name='brand_all'),

    url(r'product/details/(?P<slug>\w+)/$', 'product_details', name='product_details'),
    url(r'boats/search/$', 'boats_search', name='boats_search'),


    url(r'ads/$', 'ads_list', name='ads_list'),
    url(r'ads/list/ajax/$', 'ads_block_list', name='ads_block_list'),
    url(r'ads/filters/ajax/$', 'ads_block_filters', name='ads_block_filters'),

    url(r'ads/detail/(?P<id>\d+)/$', 'ads_detail', name='ads_detail'),
    url(r'ads/add/$', 'ad_add', name='ad_add'),
    url(r'ads/edit/(?P<id>\d+)/$', 'ad_edit', name='ad_edit'),
    url(r'ads/add/filter/ajax/$', 'ads_add_filter', name='ads_add_filter'),
    url(r'ads/del/filter/(?P<type>\w+)/$', 'ads_del_filter', name='del'),

    url(r'ad/image/delete/(?P<pk>\d+)/$', 'adimage_delete', name='adimage_delete'),
    #url(r'^$', 'image_view', name='main'),
    #url(r'^multi/$', 'multiuploader', name='multi'),

    url(r'^search/model/ajax/$', 'search_model_ajax', name='search_model_ajax'),
    url(r'^ad/country/ajax/$', 'country_select_ajax', name='country_select_ajax'),
    url(r'^admin/product/type/ajax/$', 'admin_type_select', name='admin_type_select'),

    url(r'^howto_buy_or_sell/$', 'howto_buy', name='howto_buy_or_sell'),
)